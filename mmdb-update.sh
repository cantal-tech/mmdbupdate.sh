#!/bin/bash
    
# Script to update maxmind databases
# Created by Pierrox
# Licensed by GPL v.2
# Last update: 29-12-2019
# --------------------------------------
# Notes: Créer un compte pour récupérer une 
# clef de license sur le site de maxminddb
# --------------------------------------

VERSION="0.1"
    
# CFG Script
# ==========
    
REPO_URL="https://download.maxmind.com/app/geoip_download"
LICENSEKEY="XXXxxxXXXxxx"
    
# Maxmind products list
PRODUCTS=("GeoLite2-Country" "GeoLite2-City")
ARCHIV_EXT="tar.gz"
DBPATH="/etc/nginx/geoip2"
    
# List dependencies
dep=(wget grep tar logger)

for prog in ${dep[@]}; do
	command -v $prog >/dev/null 2>&1 || {
		echo "*${prog}* seems to be unavailables : Abort." >&2
		exit 1;
	}
done
    
mkdir -p ${DBPATH}
    
# START SCRIPT
# =============
    
for PRODUCT in ${PRODUCTS[*]}; do
    wget -q "${REPO_URL}?edition_id=${PRODUCT}&license_key=${LICENSEKEY}&suffix=${ARCHIV_EXT}" -o /dev/null -O ${DBPATH}/${PRODUCT}.${ARCHIV_EXT} \
    && need="$(tar tf ${DBPATH}/${PRODUCT}.${ARCHIV_EXT} | grep ${PRODUCT}.mmdb)" \
    && tar -xf "${DBPATH}"/${PRODUCT}.${ARCHIV_EXT} -C  "${DBPATH}" "${need}" --strip 1 2>/dev/null \
    && logger "${PRODUCT}.mmdb update" \
    || logger "${PRODUCT}.mmdb update fail" -p 4
done

